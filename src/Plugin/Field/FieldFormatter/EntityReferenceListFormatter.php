<?php

namespace Drupal\ledger\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a formatter that renders a list of entities using its list builder.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_list",
 *   label = @Translation("Entity list"),
 *   description = @Translation("Display the referenced entities in a list."),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class EntityReferenceListFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a entity reference list formatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'limit' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['limit'] = [
      '#type' => 'number',
      '#title' => t('Limit'),
      '#default_value' => $this->getSetting('limit'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Limit: @limit', ['@limit' => $this->getSetting('limit')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [0 => []];

    $target_entity_type_id = $items->getFieldDefinition()->getFieldStorageDefinition()->getSetting('target_type');

    /* @var \Drupal\Core\Entity\EntityListBuilder $target_list_builder */
    $target_entity_type = $this->entityTypeManager->getDefinition($target_entity_type_id);
    $target_list_builder = $this->entityTypeManager->getListBuilder($target_entity_type_id);

    /* @see \Drupal\Core\Entity\EntityListBuilder::render() */
    $elements[0]['table'] = [
      '#type' => 'table',
      '#header' => $target_list_builder->buildHeader(),
      '#title' => $this->getSetting('title'),
      '#rows' => [],
      '#empty' => $this->t('There is no @label yet.', ['@label' => $target_entity_type->getLabel()]),
      '#cache' => [
        'contexts' => $target_entity_type->getListCacheContexts(),
        'tags' => $target_entity_type->getListCacheTags(),
      ],
    ];
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($row = $target_list_builder->buildRow($entity)) {
        $elements[0]['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->getSetting('limit')) {
      $elements[0]['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that have a list
    // builder.
    $field_storage_definition = $field_definition->getFieldStorageDefinition();$target_entity_type_id = $field_storage_definition->getSetting('target_type');
    $target_entity_type = \Drupal::entityTypeManager()->getDefinition($target_entity_type_id);
    return $field_storage_definition->isMultiple()
      && $target_entity_type->hasListBuilderClass()
      && is_a($target_entity_type->getListBuilderClass(), EntityListBuilder::class, TRUE);
  }

}