<?php

namespace Drupal\ledger\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;

/**
 * Plugin implementation of the 'number_decimal_inverse' formatter.
 *
 * @FieldFormatter(
 *   id = "number_decimal_inverse",
 *   label = @Translation("Inverse"),
 *   field_types = {
 *     "decimal",
 *     "float"
 *   }
 * )
 *
 * @todo Provide formatters using BC-math.
 */
class InverseDecimalFormatter extends DecimalFormatter {

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
    return number_format(-1 * $number, $this->getSetting('scale'), $this->getSetting('decimal_separator'), $this->getSetting('thousand_separator'));
  }

}
