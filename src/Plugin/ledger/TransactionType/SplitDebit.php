<?php

namespace Drupal\ledger\Plugin\ledger\TransactionType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\ledger\BundlePlugin\BundlePluginBase;

/**
 * @TransactionType(
 *   id = "split_debit",
 *   label = @Translation("Debit split"),
 * )
 */
class SplitDebit extends BundlePluginBase {

  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    $fields['credit'] = BundleFieldDefinition::createFromFieldStorageDefinition($base_field_definitions['credit'])
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'amount',
        ],
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'amount',
        ],
        'weight' => 30,
      ]);

    return $fields;
  }

  public function buildEntity(EntityInterface $transaction) {
    /* @var \Drupal\ledger\Entity\Transaction $transaction */
    foreach ($transaction->getCredit() as $entry) {
      $entry->set('description', $transaction->getDescription());
    }
  }

}
