<?php

namespace Drupal\ledger\Plugin\ledger\TransactionType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\ledger\BundlePlugin\BundlePluginBase;
use Drupal\ledger\Entity\Entry;

/**
 * @TransactionType(
 *   id = "simple",
 *   label = @Translation("Simple"),
 * )
 */
class Simple extends BundlePluginBase {

  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions */
    $fields['debit'] = BundleFieldDefinition::createFromFieldStorageDefinition($base_field_definitions['debit'])
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'amount',
        ],
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'amount',
        ],
        'weight' => 20,
      ]);

    $fields['credit'] = BundleFieldDefinition::createFromFieldStorageDefinition($base_field_definitions['credit'])
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'account',
        ],
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'account',
        ],
        'weight' => 30,
      ]);

    return $fields;
  }

  public function buildEntity(EntityInterface $transaction) {
    /* @var \Drupal\ledger\Entity\Transaction $transaction */
    $amount = '0';
    foreach ($transaction->getDebit() as $debit_entry) {
      $debit_entry->set('description', $transaction->getDescription());
      $amount = $debit_entry->getAmount();
    }
    foreach ($transaction->getCredit() as $credit_entry) {
      $credit_entry
        ->set('description', $transaction->getDescription())
        ->set('amount', bcmul('-1', $amount, Entry::SCALE));
    }
  }

}
