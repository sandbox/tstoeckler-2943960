<?php

namespace Drupal\ledger\Plugin\ledger\TransactionType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\ledger\BundlePlugin\BundlePluginBase;

/**
 * @TransactionType(
 *   id = "split_credit",
 *   label = @Translation("Credit split"),
 * )
 */
class SplitCredit extends BundlePluginBase {

  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    $fields['debit'] = BundleFieldDefinition::createFromFieldStorageDefinition($base_field_definitions['debit'])
    ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'amount',
        ],
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'amount',
        ],
        'weight' => 20,
      ]);

    return $fields;
  }

  public function buildEntity(EntityInterface $transaction) {
    /* @var \Drupal\ledger\Entity\Transaction $transaction */
    foreach ($transaction->getDebit() as $entry) {
      $entry->set('description', $transaction->getDescription());
    }
  }

}
