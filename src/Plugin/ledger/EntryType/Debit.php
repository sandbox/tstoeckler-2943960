<?php

namespace Drupal\ledger\Plugin\ledger\EntryType;

use Drupal\ledger\BundlePlugin\BundlePluginBase;
use Drupal\ledger\Entity\Transaction;

/**
 * Provides the debit entry type.
 *
 * @EntryType(
 *   id = "debit",
 *   label = @Translation("Debit"),
 * )
 */
class Debit extends BundlePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = [];

    $fields['amount'] = clone $base_field_definitions['amount'];
    $fields['amount']->addPropertyConstraints('value', [
      'BcMoreThan' => [
        'number' => 0,
        'scale' => Transaction::SCALE,
      ],
    ]);

    return $fields;
  }

}
