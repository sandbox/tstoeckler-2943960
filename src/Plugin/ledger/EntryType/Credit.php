<?php

namespace Drupal\ledger\Plugin\ledger\EntryType;

use Drupal\ledger\BundlePlugin\BundlePluginBase;
use Drupal\ledger\Entity\Transaction;

/**
 * Provides the credit entry type.
 *
 * @EntryType(
 *   id = "credit",
 *   label = @Translation("Credit"),
 * )
 */
class Credit extends BundlePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = [];

    $fields['amount'] = clone $base_field_definitions['amount'];
    $fields['amount']->addPropertyConstraints('value', [
      'BcLessThan' => [
        'number' => 0,
        'scale' => Transaction::SCALE,
      ],
    ]);

    return $fields;
  }

}
