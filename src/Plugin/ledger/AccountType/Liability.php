<?php

namespace Drupal\ledger\Plugin\ledger\AccountType;

use Drupal\ledger\BundlePlugin\BundlePluginBase;
use Drupal\ledger\Entity\Entry;

/**
 * Provides the liability account type.
 *
 * @AccountType(
 *   id = "liability",
 *   label = @Translation("Liability"),
 * )
 */
class Liability extends BundlePluginBase {

  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields['balance'] = clone $base_field_definitions['balance'];
    $fields['balance']->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'number_decimal_inverse',
      'settings' => [
        'scale' => Entry::SCALE,
      ],
      'weight' => 30,
    ]);

    return $fields;
  }

}
