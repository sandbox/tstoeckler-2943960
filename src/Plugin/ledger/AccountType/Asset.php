<?php

namespace Drupal\ledger\Plugin\ledger\AccountType;

use Drupal\ledger\BundlePlugin\BundlePluginBase;

/**
 * Provides the asset account type.
 *
 * @AccountType(
 *   id = "asset",
 *   label = @Translation("Asset"),
 * )
 */
class Asset extends BundlePluginBase {

}
