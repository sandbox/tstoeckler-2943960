<?php

namespace Drupal\ledger\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\ledger\Annotation\TransactionType;
use Drupal\ledger\BundlePlugin\BundlePluginInterface;

/**
 * Provides a plugin manager for transaction types.
 */
class TransactionTypeManager extends DefaultPluginManager {

  /**
   * Constructs a transaction type manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ledger/TransactionType', $namespaces, $module_handler, BundlePluginInterface::class, TransactionType::class);
    $this->alterInfo('ledger_transaction_type_info');
    $this->setCacheBackend($cache_backend, 'ledger_transaction_type_plugins');
  }

}
