<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BcMoreThanConstraintValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof BcMoreThanConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\BcMoreThanConstraint');
    }
    if (!is_string($value)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    if (bccomp($value, $constraint->number, $constraint->scale) < 1) {
      $this->context->addViolation($constraint->notMoreThan, ['%number' => $constraint->number]);
    }
  }

}
