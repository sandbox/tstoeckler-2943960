<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be unequal to a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "ValidatedReference",
 *   label = @Translation("Validated entity reference"),
 * )
 *
 * @see bccomp()
 */
class ValidatedReferenceConstraint extends Constraint {

  public $scale;

  public $invalidMessage = 'The value must be an entity reference field.';
  public $invalidatableMessage = 'The referenced entity cannot be validated.';

}
