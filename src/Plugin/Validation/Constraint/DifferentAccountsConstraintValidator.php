<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DifferentAccountsConstraintValidator extends ConstraintValidator {

  /**
   * @param mixed $value
   * @param \Symfony\Component\Validator\Constraint $constraint
   */
  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof DifferentAccountsConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DifferentAccountsConstraint');
    }
    if (!($value instanceof EntityReferenceFieldItemListInterface) || ($value->getSetting('target_type') !== 'ledger_entry')) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    $account_ids = [];
    /* @var \Drupal\ledger\Entity\Entry $entry */
    foreach ($value->referencedEntities() as $entry) {
      $account = $entry->getAccount();
      if (!$account) {
        continue;
      }

      if (in_array($account->id(), $account_ids, TRUE)) {
        $this->context->addViolation($constraint->equalMessage);
        break;
      }

      $account_ids[] = $account->id();
    }
  }

}
