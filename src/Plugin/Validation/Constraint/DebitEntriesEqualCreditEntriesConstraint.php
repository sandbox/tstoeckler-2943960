<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be unequal to a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "DebitEntriesEqualCreditEntries",
 *   label = @Translation("Debit equals credit"),
 * )
 *
 * @see bccomp()
 */
class DebitEntriesEqualCreditEntriesConstraint extends Constraint {

  public $scale;

  public $invalidMessage = 'The value must be a transaction.';
  public $unequalMessage = 'The debit amount must match the credit amount.';

}
