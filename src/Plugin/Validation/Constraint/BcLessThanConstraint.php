<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be less than a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "BcLessThan",
 *   label = @Translation("Less than (compared using arbitrary-precision arithmetic)"),
 * )
 *
 * @see bccomp()
 */
class BcLessThanConstraint extends Constraint {

  public $number;

  public $scale;

  public $invalidMessage = 'The value to compare must be a string.';
  public $notLessThan = 'The value should be less than %number.';

}
