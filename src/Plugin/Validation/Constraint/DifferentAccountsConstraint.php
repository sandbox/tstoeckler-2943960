<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be unequal to a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "DifferentAccounts",
 *   label = @Translation("Different accounts"),
 * )
 *
 * @see bccomp()
 */
class DifferentAccountsConstraint extends Constraint {

  public $scale;

  public $invalidMessage = 'The value must be a transaction reference field.';
  public $equalMessage = 'The referenced transactions may not reference the same account.';

}
