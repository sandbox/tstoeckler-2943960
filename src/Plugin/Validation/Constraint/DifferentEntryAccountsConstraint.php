<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be unequal to a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "DifferentEntryAccounts",
 *   label = @Translation("Debit accounts are different from credit accounts"),
 * )
 *
 * @see bccomp()
 */
class DifferentEntryAccountsConstraint extends Constraint {

  public $invalidMessage = 'The value must be a transaction.';
  public $equalMessage = 'All credit accounts must be different from the debit accounts.';

}
