<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Drupal\ledger\Entity\Transaction;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DifferentEntryAccountsConstraintValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof DifferentEntryAccountsConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DifferentEntryAccountsConstraint');
    }
    if (!($value instanceof Transaction)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    $account_ids = [];
    /* @var \Drupal\ledger\Entity\Entry[] $entries */
    $entries = array_merge($value->getDebit(), $value->getCredit());
    foreach ($entries as $entry) {
      $account = $entry->getAccount();
      if (!$account) {
        continue;
      }

      if (in_array($account->id(), $account_ids, TRUE)) {
        $this->context->addViolation($constraint->equalMessage);
        break;
      }

      $account_ids[] = $account->id();
    }
  }

}
