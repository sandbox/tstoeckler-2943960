<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be unequal to a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "BcNotEqualTo",
 *   label = @Translation("Not equal (compared using arbitrary-precision arithmetic)"),
 * )
 *
 * @see bccomp()
 */
class BcNotEqualToConstraint extends Constraint {

  public $number;

  public $scale;

  public $invalidMessage = 'The value to compare must be a string.';
  public $equalMessage = 'The value should not be equal to %number.';

}
