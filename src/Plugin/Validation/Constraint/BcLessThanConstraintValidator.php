<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BcLessThanConstraintValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof BcLessThanConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\BcLessThanConstraint');
    }
    if (!is_string($value)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    if (bccomp($value, $constraint->number, $constraint->scale) > -1) {
      $this->context->addViolation($constraint->notLessThan, ['%number' => $constraint->number]);
    }
  }

}
