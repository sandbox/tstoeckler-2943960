<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Drupal\ledger\Entity\Entry;
use Drupal\ledger\Entity\Transaction;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DebitEntriesEqualCreditEntriesConstraintValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof DebitEntriesEqualCreditEntriesConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DebitEntriesEqualCreditEntriesConstraint');
    }
    if (!($value instanceof Transaction)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    $sum = '0';
    foreach (['debit', 'credit'] as $field_name) {
      /* @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $entry_items */
      $entry_items = $value->get($field_name);
      /* @var \Drupal\ledger\Entity\Entry $entry */
      foreach ($entry_items->referencedEntities() as $entry) {
        $sum = bcadd($sum, $entry->getAmount(), Entry::SCALE);
      }
    }

    if (bccomp($sum, '0', $constraint->scale) !== 0) {
      $this->context->addViolation($constraint->unequalMessage);
    }
  }

}
