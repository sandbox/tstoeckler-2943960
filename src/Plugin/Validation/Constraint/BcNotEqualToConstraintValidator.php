<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BcNotEqualToConstraintValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof BcNotEqualToConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\BcNotEqualToConstraint');
    }
    if (!is_string($value)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    if (bccomp($value, $constraint->number, $constraint->scale) === 0) {
      $this->context->addViolation($constraint->equalMessage, ['%number' => $constraint->number]);
    }
  }

}
