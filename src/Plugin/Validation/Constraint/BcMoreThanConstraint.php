<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for a numeric string to be less than a given value.
 *
 * The comparison is done using arbitrary-precision arithmetic.
 *
 * @Constraint(
 *   id = "BcMoreThan",
 *   label = @Translation("More than (compared using arbitrary-precision arithmetic)"),
 * )
 *
 * @see bccomp()
 */
class BcMoreThanConstraint extends Constraint {

  public $number;

  public $scale;

  public $invalidMessage = 'The value to compare must be a string.';
  public $notMoreThan = 'The value should be more than %number.';

}
