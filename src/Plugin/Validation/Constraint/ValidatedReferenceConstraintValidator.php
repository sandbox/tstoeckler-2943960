<?php

namespace Drupal\ledger\Plugin\Validation\Constraint;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidatedReferenceConstraintValidator extends ConstraintValidator {

  /**
   * @param mixed $value
   * @param \Symfony\Component\Validator\Constraint $constraint
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof ValidatedReferenceConstraint)) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\ValidatedReferenceConstraint');
    }
    if (!($value instanceof EntityReferenceFieldItemListInterface)) {
      $this->context->addViolation($constraint->invalidMessage);
      return;
    }

    foreach ($value->referencedEntities() as $delta => $entity) {
      if (!($entity instanceof FieldableEntityInterface)) {
        $this->context->buildViolation($constraint->invalidatableMessage)
          ->atPath("$delta.entity")
          ->setInvalidValue($entity)
          ->addViolation();
        continue;
      }

      /* @var \Symfony\Component\Validator\ConstraintViolation $violation */
      foreach ($entity->validate() as $violation) {
        $property_path = "$delta.entity";
        if ($sub_path = $violation->getPropertyPath()) {
          $property_path .= ".$sub_path";
        }
        $this->context->buildViolation($violation->getMessageTemplate(), $violation->getParameters())
          ->atPath($property_path)
          ->addViolation();
      }
    }
  }

}
