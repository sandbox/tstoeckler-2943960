<?php

namespace Drupal\ledger\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation class for transaction types.
 *
 * @Annotation
 */
class TransactionType extends Plugin {

  public $id;

  public $label;

}
