<?php

namespace Drupal\ledger\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation class for account types.
 *
 * @Annotation
 */
class AccountType extends Plugin {

  public $id;

  public $label;

}
