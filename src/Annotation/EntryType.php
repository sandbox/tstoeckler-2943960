<?php

namespace Drupal\ledger\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation class for entry types.
 *
 * @Annotation
 */
class EntryType extends Plugin {

  public $id;

  public $label;

}
