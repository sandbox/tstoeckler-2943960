<?php

namespace Drupal\ledger\Form;

use Drupal\Core\Entity\ContentEntityForm as BaseContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ledger\BundlePlugin\BundlePluginInterface;

/**
 * Provides a form for ledger content entities.
 */
class ContentEntityForm extends BaseContentEntityForm {

  public function buildEntity(array $form, FormStateInterface $form_state) {
    $entity = parent::buildEntity($form, $form_state);

    if ($bundle_plugin_type = $entity->getEntityType()->get('bundle_plugin_type')) {
      // @todo Consider adding a dedicated bundle plugin handler instead of
      //   accessing the plugin manager directly.
      /* @var \Drupal\Component\Plugin\PluginManagerInterface $bundle_plugin_manager */
      $bundle_plugin_manager = \Drupal::service("plugin.manager.$bundle_plugin_type");
      $bundle_plugin = $bundle_plugin_manager->createInstance($entity->bundle());
      if ($bundle_plugin instanceof BundlePluginInterface) {
        $bundle_plugin->buildEntity($entity);
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $entity = $this->getEntity();

    if ($saved === SAVED_NEW) {
      $this->logger('ledger')->notice('@type: added %label.', [
        '@type' => $entity->getEntityTypeId(),
        '%label' => $entity->label(),
      ]);
      $this->messenger()->addStatus($this->t('@type %label has been created.', [
        '@type' => $entity->getEntityType()->getLabel(),
        '%label' => $entity->label(),
      ]));
    }
    else {
      $this->logger('ledger')->notice('@type: updated %label.', [
        '@type' => $entity->getEntityTypeId(),
        '%label' => $entity->label(),
      ]);
      $this->messenger()->addStatus($this->t('@type %label has been updated.', [
        '@type' => $entity->getEntityType()->getLabel(),
        '%label' => $entity->label(),
      ]));
    }

    if ($entity->hasLinkTemplate('collection')) {
      $form_state->setRedirectUrl($entity->toUrl('collection'));
    }

    return $saved;
  }

}
