<?php

namespace Drupal\ledger\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\nested_set\Entity\NestedSetListBuilder;

/**
 * Provides a list builder for accounts.
 */
class AccountListBuilder extends NestedSetListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $limit = 50;

  /**
   * {@inheritdoc}
   */
  protected $nestedSetFieldName = 'hierarchy';

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Label'),
      'type' => $this->t('Type'),
      'balance' => $this->t('Balance'),
    ];
    $header += parent::buildHeader();

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $row = [];
    $field_names = array_keys($this->buildHeader());
    foreach ($field_names as $field_name) {
      // Support non-field columns, such as 'operations'.
      if (!$entity->hasField($field_name)) {
        continue;
      }

      $items = $entity->get($field_name);

      $field_definition = $items->getFieldDefinition();

      $display_options = ['label' => 'hidden'];
      if ($field_display_options = $field_definition->getDisplayOptions('view')) {
        $display_options += $field_display_options;
      }

      $row[$field_name] = $items->view($display_options);
      $is_multiple = $field_definition->getFieldStorageDefinition()->isMultiple();
      // Avoid unnecessary field wrapper markup for single-value fields. This is
      // important for the label field, as the Field theme function breaks the
      // various tabledrag additions.
      if (!$is_multiple && isset($row[$field_name][0])) {
        $row[$field_name] = $row[$field_name][0];
      }
    }
    $row += parent::buildRow($entity);

    return $row;
  }

}
