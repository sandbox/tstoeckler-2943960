<?php

namespace Drupal\ledger\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list builder for entries.
 */
class EntryListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'description' => $this->t('Description'),
      'account' => $this->t('Account'),
      'amount' => $this->t('Amount'),
    ];

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $row = [];
    $field_names = array_keys($this->buildHeader());
    foreach ($field_names as $field_name) {
      $items = $entity->get($field_name);
      $display_options = ['label' => 'hidden'];
      if ($field_display_options = $items->getFieldDefinition()->getDisplayOptions('view')) {
        $display_options += $field_display_options;
      }
      $row[$field_name] = [
        'data' => $items->view($display_options),
      ];
    }
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery();
    $query
      ->accessCheck()
      ->sort($this->entityType->getKey('id'), 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
