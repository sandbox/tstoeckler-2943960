<?php

namespace Drupal\ledger\Entity;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\ledger\Field\BalanceFieldItemList;
use Drupal\ledger\Field\EntriesFieldItemList;

/**
 * Provides an account entity.
 *
 * @todo Make revisionable.
 * @todo Prevent editing and deleting accounts if there are associated entries.
 *
 * @ContentEntityType(
 *   id = "ledger_account",
 *   label = @Translation("Account"),
 *   label_collection = @Translation("Accounts"),
 *   translatable = TRUE,
 *   base_table = "ledger_account",
 *   data_table = "ledger_account_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "label",
 *     "published" = "status",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\ledger\Controller\AccountListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "add" = "Drupal\ledger\Form\ContentEntityForm",
 *       "edit" = "Drupal\ledger\Form\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/structure/ledger/accounts",
 *     "add-page" = "/admin/structure/ledger/accounts/add",
 *     "add-form" = "/admin/structure/ledger/accounts/add/{type}",
 *     "canonical" = "/admin/structure/ledger/accounts/manage/{ledger_account}",
 *     "edit-form" = "/admin/structure/ledger/accounts/manage/{ledger_account}/edit",
 *     "delete-form" = "/admin/structure/ledger/accounts/manage/{ledger_account}/delete",
 *   },
 *   bundle_plugin_type = "ledger.account_type",
 *   admin_permission = "administer ledger_account",
 * )
 */
class Account extends ContentEntityBase implements EntityPublishedInterface {

  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('label')]
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'settings' => [
          'link_to_entity' => TRUE,
        ],
        'weight' => 0,
      ]);

    $fields['type']
      ->setLabel(new TranslatableMarkup('Type'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 10
      ]);

    $fields['hierarchy'] = BaseFieldDefinition::create('nested_set')
      ->setLabel(new TranslatableMarkup('Hierarchy'))
      ->setRequired(TRUE);

    $fields['balance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('Balance'))
      /* @see \Drupal\ledger\Entity\Transaction::baseFieldDefinitions() */
      ->setSetting('precision', 19)
      ->setSetting('scale', 6)
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass(BalanceFieldItemList::class)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => Entry::SCALE,
        ],
        'weight' => 20,
      ]);

    $fields['debit'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Debit'))
      ->setSetting('target_type', 'ledger_entry')
      ->setSetting('handler_settings', [
        'target_bundles' => ['debit'],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass(EntriesFieldItemList::class)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_list',
        'weight' => 30,
      ]);

    $fields['credit'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Credit'))
      ->setSetting('target_type', 'ledger_entry')
      ->setSetting('handler_settings', [
        'target_bundles' => ['credit'],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass(EntriesFieldItemList::class)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_list',
        'weight' => 40,
      ]);

    return $fields;
  }

}
