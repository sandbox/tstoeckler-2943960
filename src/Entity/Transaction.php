<?php

namespace Drupal\ledger\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a transaction entity.
 *
 * @todo Prevent editing and deleting transactions via the API.
 *
 * @ContentEntityType(
 *   id = "ledger_transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Journal"),
 *   base_table = "ledger_transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "description",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\ledger\Controller\TransactionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ledger\Form\ContentEntityForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   constraints = {
 *     "DebitEntriesEqualCreditEntries" = {
 *       "scale" = 6,
 *     },
 *     "DifferentEntryAccounts" = {},
 *   },
 *   links = {
 *     "collection" = "/admin/structure/ledger/journal",
 *     "add-page" = "/admin/structure/ledger/journal/add-transaction",
 *     "add-form" = "/admin/structure/ledger/journal/add-transaction/{type}",
 *   },
 *   bundle_plugin_type = "ledger.transaction_type",
 *   admin_permission = "administer ledger_transaction",
 * )
 */
class Transaction extends ContentEntityBase {

  const SCALE = 6;

  /**
   * @return string
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * @return \Drupal\ledger\Entity\Entry[]
   */
  public function getDebit() {
    /* @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $debit_items */
    $debit_items = $this->get('debit');
    return $debit_items->referencedEntities();
  }

  /**
   * @return \Drupal\ledger\Entity\Entry[]
   */
  public function getCredit() {
    /* @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $credit_items */
    $credit_items = $this->get('credit');
    return $credit_items->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type']
      ->setLabel(new TranslatableMarkup('Type'));

    $fields[$entity_type->getKey('label')]
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ]);

    $fields['debit'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Debit'))
      ->setSetting('target_type', 'ledger_entry')
      ->setSetting('handler_settings', [
        'target_bundles' => ['debit'],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      //->addConstraint('ValidatedReference')
      ->addConstraint('DifferentAccounts')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'weight' => 20,
      ]);

    $fields['credit'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Credit'))
      ->setSetting('target_type', 'ledger_entry')
      ->setSetting('handler_settings', [
        'target_bundles' => ['credit'],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      //->addConstraint('ValidatedReference')
      ->addConstraint('DifferentAccounts')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'weight' => 30,
      ]);

    // @todo Add timezone support.
    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(new TranslatableMarkup('Date'))
      ->setRequired(TRUE)
      ->setDefaultValueCallback(static::class . '::getRequestTime')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 10,
      ]);

    // @todo Store a reference to an invoice or bank record.

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequestTime() {
    // Convert the request time from a Unix timestamp to the datetime storage
    // format.
    /* @link http://php.net/manual/en/function.date.php */
    $timestamp = \Drupal::time()->getRequestTime();
    $timezone = new \DateTimeZone('UTC');
    $date = \DateTime::createFromFormat('U', $timestamp, $timezone);
    return $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

}
