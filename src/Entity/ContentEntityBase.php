<?php

namespace Drupal\ledger\Entity;

use Drupal\Core\Entity\ContentEntityBase as BaseContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\ledger\BundlePlugin\BundlePluginInterface;

abstract class ContentEntityBase extends BaseContentEntityBase {

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    if ($entity_type->hasKey('label')) {
      $label_key = $entity_type->getKey('label');
      $fields[$label_key] = BaseFieldDefinition::create('string')
        ->setLabel(t('Label'))
        ->setRequired(TRUE)
        ->setDisplayOptions('form', [
          'weight' => 0,
        ]);
      if ($entity_type->isTranslatable()) {
        $fields[$label_key]->setTranslatable(TRUE);
      }
    }

    if ($entity_type->hasKey('bundle') && !$entity_type->getBundleEntityType()) {
      $bundle_key = $entity_type->getKey('bundle');
      $fields[$bundle_key] = BaseFieldDefinition::create('list_string')
        ->setLabel(t('Type'))
        ->setSetting('allowed_values_function', [static::class, 'getBundleOptions'])
        ->setRequired(TRUE)
        ->setReadOnly(TRUE);
    }


    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];
    if ($bundle_plugin_type = $entity_type->get('bundle_plugin_type')) {
      // @todo Consider adding a dedicated bundle plugin handler instead of
      //   accessing the plugin manager directly.
      /* @var \Drupal\Component\Plugin\PluginManagerInterface $bundle_plugin_manager */
      $bundle_plugin_manager = \Drupal::service("plugin.manager.$bundle_plugin_type");
      $bundle_plugin = $bundle_plugin_manager->createInstance($bundle);
      if ($bundle_plugin instanceof BundlePluginInterface) {
        $fields = $bundle_plugin->buildBundleFieldDefinitions($base_field_definitions);
      }
    }
    return $fields;
  }

  public static function getBundleOptions() {
    /* @var \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository */
    $entity_type_repository = \Drupal::service('entity_type.repository');
    /* @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
    $bundle_info = \Drupal::service('entity_type.bundle.info');

    $entity_type_id = $entity_type_repository->getEntityTypeFromClass(static::class);
    $bundle_options = [];
    foreach ($bundle_info->getBundleInfo($entity_type_id) as $bundle => $info) {
      $bundle_options[$bundle] = $info['label'];
    }
    return $bundle_options;
  }

}
