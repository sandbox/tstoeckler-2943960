<?php

namespace Drupal\ledger\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an entry entity.
 *
 * @todo Prevent editing and deleting entries via the API.
 *
 * @ContentEntityType(
 *   id = "ledger_entry",
 *   label = @Translation("Entry"),
 *   base_table = "ledger_entry",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "description",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\ledger\Controller\EntryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ledger\Form\ContentEntityForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   links = {
 *     "add-form" = "/admin/entries/{bundle}",
 *   },
 *   bundle_plugin_type = "ledger.entry_type",
 *   admin_permission = "administer ledger_entry",
 *   field_ui_base_route = "entity.ledger_entry.add_form",
 * )
 */
class Entry extends ContentEntityBase {

  const SCALE = 6;

  /**
   * @return string
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * @return \Drupal\ledger\Entity\Account
   */
  public function getAccount() {
    return $this->get('account')->entity;
  }

  /**
   * @return string
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('label')]
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['type']
      ->setLabel(t('Type'));

    $fields['account'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Account'))
      ->setSetting('target_type', 'ledger_account')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      //->addConstraint('ValidatedReference')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @todo Store the currency.
    $fields['amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('Amount'))
      /* @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem::schema() */
      // @todo Find out and document why these values were chosen.
      ->setSetting('precision', 19)
      ->setSetting('scale', static::SCALE)
      ->setRequired(TRUE)
      ->addPropertyConstraints('value', [
        'BcNotEqualTo' => [
          'number' => '0',
          'scale' => static::SCALE,
        ],
      ])
      ->setDisplayOptions('form', [
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => static::SCALE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
