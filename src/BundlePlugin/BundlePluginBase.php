<?php

namespace Drupal\ledger\BundlePlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;

abstract class BundlePluginBase extends PluginBase implements BundlePluginInterface {

  public function buildFieldDefinitions() {
    return [];
  }

  public function buildBundleFieldDefinitions(array $base_field_definitions) {
    return [];
  }

  public function buildEntity(EntityInterface $entity) {
  }

}
