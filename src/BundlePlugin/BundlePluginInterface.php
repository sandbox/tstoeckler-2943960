<?php

namespace Drupal\ledger\BundlePlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity\BundlePlugin\BundlePluginInterface as BaseBundlePluginInterface;

/**
 * Provides an interface for bundle plugins.
 */
interface BundlePluginInterface extends BaseBundlePluginInterface {

  /**
   * Builds the field definitions for entities of this bundle.
   *
   * Important:
   * Field names must be unique across all bundles.
   * It is recommended to prefix them with the bundle name (plugin ID).
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $base_field_definitions
   *   The list of base field definitions.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name.
   *
   * @see \Drupal\Core\Entity\FieldableEntityInterface::bundleFieldDefinitions()
   */
  public function buildBundleFieldDefinitions(array $base_field_definitions);

  public function buildEntity(EntityInterface $entity);

}
