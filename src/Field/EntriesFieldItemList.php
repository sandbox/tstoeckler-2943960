<?php

namespace Drupal\ledger\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * An item list class for the entries of an account.
 */
class EntriesFieldItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function computeValue() {
    $entry_storage = \Drupal::entityTypeManager()->getStorage('ledger_entry');

    /* @var \Drupal\ledger\Entity\Account $account */
    $account = $this->getEntity();
    assert($account->getEntityTypeId() === 'ledger_account');

    $query = $entry_storage->getQuery()
      ->accessCheck()
      ->condition('account.entity.hierarchy.lft', $account->get('hierarchy')->lft, '>=')
      ->condition('account.entity.hierarchy.rgt', $account->get('hierarchy')->rgt, '<=');
    $handler_settings = $this->getSetting('handler_settings');
    if (!empty($handler_settings['target_bundles'])) {
      $query->condition('type', $handler_settings['target_bundles'], 'IN');
    }

    // @todo Sort by date.
    $entry_ids = array_values($query->execute());
    foreach ($entry_storage->loadMultiple($entry_ids) as $delta => $entry) {
      $this->list[$delta] = $this->createItem($delta, $entry);
    }
  }

}
