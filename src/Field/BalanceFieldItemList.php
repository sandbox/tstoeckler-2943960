<?php

namespace Drupal\ledger\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * An item list class for the balance of an account.
 *
 * @todo Evaluate whether we need to ensure to always have a field item like
 *   ModerationStateFieldItemList.
 */
class BalanceFieldItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function computeValue() {
    $entry_storage = \Drupal::entityTypeManager()->getStorage('ledger_entry');

    /* @var \Drupal\ledger\Entity\Account $account */
    $account = $this->getEntity();
    assert($account->getEntityTypeId() === 'ledger_account');

    $alias = 'amount_sum';
    $result = $entry_storage->getAggregateQuery()
      ->accessCheck(FALSE)
      ->aggregate('amount', 'SUM', NULL, $alias)
      ->condition('account.entity.hierarchy.lft', $account->get('hierarchy')->lft, '>=')
      ->condition('account.entity.hierarchy.rgt', $account->get('hierarchy')->rgt, '<=')
      ->execute();

    $value = $result[0][$alias];
    if ($result[0][$alias] === NULL) {
      $value = 0;
    }
    $this->list[0] = $this->createItem(0, $value);
  }

}
